/*
 * Tan-Jackson Tran
 * 1936038
 */

package application;

import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] arr = new Bicycle[4] ;
        arr[0] = new Bicycle("Sony", 20, 20);
        arr[1] = new Bicycle("Apple", 30, 30);
        arr[2] = new Bicycle("Samsung", 40, 40);
        arr[3] = new Bicycle("Beats", 50, 50);

        for (Bicycle a : arr) {
            System.out.println(a.toString());
        }
        
    
    }

}